$(document).ready(function(){
	$('.center').slick({
  	infinite: true,
  	speed: 3000,
    autoplay: true,
    autoplaySpeed: 1500,
  	slidesToShow: 1,
  	centerMode: true,
  	variableWidth: true
	});
});